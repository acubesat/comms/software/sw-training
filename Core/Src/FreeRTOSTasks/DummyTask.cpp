#include "DummyTask.hpp"

void DummyTask::execute() {
    while (true) {
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}